<?php

// Mga kahulugang ginamit sa userlist.php para sa wikang ito
$lang_ul = array(

'User find legend'	=>	'Humanap at isaayos ang mga tagagamit',
'User search info'	=>	'Magpasok ng isang pangalan ng tagagamit na hahanapin at/o pangkat na pangsala. Puwede maging blanko ang patlang para sa pangalan ng tagagamit. Gamitin ang * para sa mga bahagyang tugma.',
'User sort info'	=>	'Isaayos ang mga tagagamit sa pamamagitan ng pangalan, petsa ng pagrehistro, o bilang ng mga post at sa pataas/pababang ayos.',
'User group'		=>	'Pangkat ng tagagamit',
'No of posts'		=>	'Bilang ng mga post',
'All users'			=>	'Lahat'

);
