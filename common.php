<?php

// Mga kahulugan para sa mga madalas gamiting mga string sa wikang ito
$lang_common = array(

// Text orientation and encoding
'lang_direction'					=>	'ltr', // ltr (Left-To-Right) or rtl (Right-To-Left), Filipino use left-to-right
'lang_identifier'					=>	'fil',

// Number formatting
'lang_decimal_point'				=>	'.',
'lang_thousands_sep'				=>	',',

// Notices
'Bad request'						=>	'Sirang bilin. Ang kawing na sinundan mo ay mali o luma.',
'No view'							=>	'Wala kang permiso para tingnan ang mga pagtitipong ito.',
'No permission'						=>	'Wala kang permiso para pumasok sa pahinang ito.',
'Bad referrer'						=>	'Sirang HTTP_REFERER. Dinala ka sa pahinang ito mula sa isang hindi awtorisadong pinagkukunan. Kung nagpupumilit parin ang problema, siguraduhin na ang \'Base URL\' ay nakaayos ng mabuti sa Admin/Options at ika'y bumibisita sa pagtitipon sa pamamgitan ng pagpunta sa URL na iyon. Ang karagdagang impormasyon tungkol sa pagsuri ng tagapagsangguni ay maaaring matagpuan sa dokumentasyon ng FluxBB.',
'Bad csrf hash'						=>	'Sirang CSRF hash. Dinala ka sa pahinang ito mula sa isang hindi awtorisadong pinagkukunan.',
'No cookie'							=>	'Mukhang ika'y nakapag-login, ngunit ang cookie ay hindi nakatakda. Pakisuri ang iyong settings at kung puwede, ay payagan ang mga cookie para sa pook-sapot na ito.',
/**
 * Masyado itong teknikal para isalin
 * (This is too technical for these to be translated)
 */
'Pun include extension'  			=>	'Unable to process user include %s from template %s. "%s" files are not allowed',
'Pun include directory'				=>	'Unable to process user include %s from template %s. Directory traversal is not allowed',
'Pun include error'					=>	'Unable to process user include %s from template %s. There is no such file in neither the template directory nor in the user include directory',
// Untranslated end

// Miscellaneous
'Announcement'						=>	'Anunsyo',
'Options'							=>	'Mga pagpipilian',
'Submit'							=>	'Ipasok', // "Name" of submit buttons
'Ban message'						=>	'Ikaw ay pinagbawalan sa pagtitipong ito.',
'Ban message 2'						=>	'Ang pagbabawal ay matatapos sa katapusan ng',
'Ban message 3'						=>	'Ang tagapangasiwa o tagapamagitan na nagbawal sa iyo ay nag-iwan ng sumusunod na mensahe:',
'Ban message 4'						=>	'Mangyaring idirekta ang mga tanong sa tagapangasiwa ng pagtitipon sa',
'Never'								=>	'Hindi kailanman',
'Today'								=>	'Ngayon',
'Yesterday'							=>	'Kahapon',
'Info'								=>	'Info', // A common table header
'Go back'							=>	'Balikan',
'Maintenance'						=>	'Pagpapanatili',
'Redirecting'						=>	'Dinidirekta',
'Click redirect'					=>	'Pindutin ito kung hindi ka makapaghintay (o kung yung browser mo ay hindi kusang pumunta sa susunod na pahina)',
'on'								=>	'bukas', // As in "BBCode is on"
'off'								=>	'patay',
'Invalid email'						=>	'Ang email address na iyong pinasok ay hindi wasto.',
'Required'							=>	'(Kinakailangan)',
'required field'					=>	'kinakailangang patlang sa form na ito.', // For javascript form validation
'Last post'							=>	'Huling paskil',
'by'								=>	'ni', // As in last post by some user
'New posts'							=>	'Mga bagong paskil', // The link that leads to the first new post
'New posts info'					=>	'Puntahan ang unang bagong paksil sa paksang ito', // The popup text for new posts links
'Username'							=>	'Pangalan ng tagagamit',
'Password'							=>	'Hudyat',
'Email'								=>	'Email',
'Send email'						=>	'Magpadala ng email',
'Moderated by'						=>	'Pinamamagitan ni',
'Registered'						=>	'Nagrehistro noong',
'Subject'							=>	'Simuno',
'Message'							=>	'Mensahe',
'Topic'								=>	'Paksa',
'Forum'								=>	'Pagtitipon',
'Posts'								=>	'Mga paskil',
'Replies'							=>	'Mga sagot',
'Pages'								=>	'Mga pahina:',
'Page'								=>	'Pahina %s',
'BBCode'							=>	'BBCode:', // Huwag palitan (Don't change this)
'url tag'							=>	'[url] tag:',
'img tag'							=>	'[img] tag:',
'Smilies'							=>	'Mga smiley:',
'and'								=>	'at', // Not to be confused with the English "at"
'Image link'						=>	'larawan', // This is displayed (i.e. <image>) instead of images when "Show images" is disabled in the profile
'wrote'								=>	'wrote:', // For [quote]'s
'Mailer'							=>	'Tagahatid-sulat ng %s', // As in "MyForums Mailer" in the signature of outgoing emails
'Important information'				=>	'Mahalagang impormasyon',
'Write message legend'				=>	'Isulat ang iyong mensahe at ipasok',
'Previous'							=>	'Nakaraan',
'Next'								=>	'Susunod',
'Spacer'							=>	'…', // Ellipsis for paginate

// Title
'Title'								=>	'Pamagat',
'Member'							=>	'Miyembro', // Default title
'Moderator'							=>	'Tagapamagitan',
'Administrator'						=>	'Tagapangasiwa',
'Banned'							=>	'Binawalan',
'Guest'								=>	'Guest',

// Stuff for include/parser.php
'BBCode error no opening tag'		=>	'Ang [/%1$s] ay natagpuang walang natutugmang [%1$s]',
'BBCode error invalid nesting'		=>	'Ang [%1$s] ay nabuksan sa loob ng [%2$s], hindi ito pinapayagan',
'BBCode error invalid self-nesting'	=>	'[%s] was opened within itself, this is not allowed Ang [%s] ay nabuksan sa loob mismo, hindi ito pinapayagan',
'BBCode error no closing tag'		=>	'Ang [%1$s] ay natagpuang walang natutugmang [/%1$s]',
'BBCode error empty attribute'		=>	'Walang laman ang bahaging katangian ng tag na [%s]',
'BBCode error tag not allowed'		=>	'Hindi ka puwedeng gumamit ng tag na [%s]',
'BBCode error tag url not allowed'	=>	'Hindi ka puwedeng magpaskil ng mga kawing',
'BBCode list size error'			=>	'Ang iyong listahan ay masyadong mahaba para iproseso, mangyaring gawin itong mas maliit!',

// Stuff for the navigator (top of every page)
'Index'								=>	'Talatuntunan',
'User list'							=>	'Listahan ng mga tagagamit',
'Rules'								=>	'Mga batas',
'Search'							=>	'Maghanap',
'Register'							=>	'Magrehistro',
'Cannot register' =>
'<a href="/viewtopic.php?pid=1736#p1736">Hindi makapagrehistro? Tingnan ito</a>.',
'Login'								=>	'Mag-login',
'Not logged in'						=>	'Hindi ka naka-login.',
'Profile'							=>	'Profile',
'Logout'							=>	'Mag-logout',
'Logged in as'						=>	'Naka-login bilang',
'Admin'								=>	'Pangangasiwa',
'Last visit'						=>	'Huling pagbisita: %s',
'Topic searches'					=>	'Mga paksa:',
'New posts header'					=>	'Bago',
'Active topics'						=>	'Aktibo',
'Unanswered topics'					=>	'Hindi pa nasagutan',
'Posted topics'						=>	'Napaskilan',
'Show new posts'					=>	'Hanapin ang mga paksang may mga bagong paksil mula sa iyong huling pagbisita.',
'Show active topics'				=>	'Hanapin ang mga paksang may mga kamakailang paskil',
'Show unanswered topics'			=>	'Hanapin ang mga paksang walang sagot.',
'Show posted topics'				=>	'Hanapin ang mga paksang pinaskilan mo.',
'Mark all as read'					=>	'Markahan lahat ng mga paksa bilang nabasa na',
'Mark forum read'					=>	'Markahan ang pagtitipong ito bilang nabasa na',
'Title separator'					=>	' / ',

// Stuff for the page footer
'Board footer'						=>	'Board footer', // Hindi makahanap ng magandang kapalit dito (Can't find a suitable translation here)
'Jump to'							=>	'Pumunta sa',
'Go'								=>	' Pasok ', // Submit button in forum jump
'Moderate topic'					=>	'Mamagitan sa paksa',
'All'					=>	'Lahat',
'Move topic'						=>	'Ilipat ang paksa',
'Open topic'						=>	'Buksan ang paksa',
'Close topic'						=>	'Isara ang paksa',
'Unstick topic'						=>	'Tanggalin sa pagkadikit ang paksa',
'Stick topic'						=>	'Idikit ang paksa',
'Moderate forum'					=>	'Mamagitan sa pagtitipon',
'Powered by'						=>	'Pinatatakbo ng %s',

// Debug information
'Debug table'						=>	'Impormasyong debug',
'Querytime'							=>	'Binuo sa loob ng %1$s segundo, nagsagawa ng %2$s na query',
'Memory usage'						=>	'Sukat ng paggamit ng memorya: %1$s',
'Peak usage'						=>	'(Pinakamataas: %1$s)', // Puwede rin ang "tuktok", pero mukhang mas malinaw ito (Could have used "tuktok", but this looks clearer)
'Query times'						=>	'Oras (s)',
'Query'								=>	'Query', // Hindi makahanap ng magandang kapalit dito (Can't find a suitable translation here)
'Total query time'					=>	'Kabuuang oras ng query: %s',

// For extern.php RSS feed
'RSS description'					=>	'The most recent topics at %s.',
'RSS description topic'				=>	'Ang mga pinakakamakailang paskil sa %s.',
'RSS reply'							=>	'Re: ', // The topic subject will be appended to this string (to signify a reply)
/**
 * Ang mga sumusunod ay hindi sinalinwika dahil walang magandang kapalit ang "feed" sa Filipino o Tagalog.
 * (The following are not translated because there are no good Filipino/Tagalog replacements for "feed".)
 */
'RSS active topics feed'			=>	'RSS active topics feed',
'Atom active topics feed'			=>	'Atom active topics feed',
'RSS forum feed'					=>	'RSS forum feed',
'Atom forum feed'					=>	'Atom forum feed',
'RSS topic feed'					=>	'RSS topic feed',
'Atom topic feed'					=>	'Atom topic feed',
// Untranslated end

// Admin related stuff in the header
'New reports'						=>	'Mayroong mga bagong ulat',
'Maintenance mode enabled'			=>	'Nakabukas ang maintenance mode!', // Translating "mode" doesn't work, so using Taglish here

// Units for file sizes, nothing to change here
'Size unit B'						=>	'%s B',
'Size unit KiB'						=>	'%s KiB',
'Size unit MiB'						=>	'%s MiB',
'Size unit GiB'						=>	'%s GiB',
'Size unit TiB'						=>	'%s TiB',
'Size unit PiB'						=>	'%s PiB',
'Size unit EiB'						=>	'%s EiB',

);
